//
// Created by Admin on 05.05.15.
// Copyright (c) 2015 nabsky. All rights reserved.
//

import Foundation
import AVFoundation

protocol AudioControllerDelegate:class {
    func audioRecordCreated(filename: String)
}

class AudioController: NSObject, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    weak var delegate: AudioControllerDelegate?

    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?

    var isRecording: Bool = false

    let documentsUrl = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as! NSURL
    var currentFile: String = ""

    var recordLength: Double = 10.0
    var recordQuality: NSNumber = AVAudioQuality.Low.rawValue
    var recordChannels: NSNumber = 1 as NSNumber
    var recordSampleRate: Double = 16000.0
    var recordFormat: NSNumber = kAudioFormatMPEG4AAC as NSNumber

    override init(){
        super.init()
        self.setupNotifications()
    }

//===Public Functions===
    func start() {
        var error: NSError?
        let session = AVAudioSession.sharedInstance()
        if session.setCategory(AVAudioSessionCategoryPlayAndRecord,
                withOptions: .MixWithOthers,
                error: &error) {
            if session.setActive(true, error: nil) {
                NSLog("Successfully activated the audio session")
                session.requestRecordPermission {
                    [weak self](allowed: Bool) in
                    if allowed {
                        self!.startRecordingAudio()
                        self!.isRecording = true
                    } else {
                        NSLog("We don't have permission to record audio");
                    }
                }
            } else {
                NSLog("Could not activate the audio session")
            }
        } else {
            if let theError = error {
                NSLog("An error occurred in setting the audio " +
                        "session category. Error = \(theError)")
            }
        }
    }

    func stopRecordingAudio() {
        isRecording = false
        audioRecorder?.pause()
    }

    func play(file: String) {
        var playbackError: NSError?
        var readingError: NSError?

        let fileData = NSData(contentsOfURL: audioPlayingPath(file),
                options: .MappedRead,
                error: &readingError)
        audioPlayer = AVAudioPlayer(data: fileData, error: &playbackError)
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        if let player = audioPlayer {
            player.delegate = self
            if player.prepareToPlay() && player.play() {
                NSLog("Started playing the recorded audio")
            } else {
                NSLog("Could not play the audio")
            }

        } else {
            NSLog("Failed to create an audio player")
        }
    }

//=================

    func audioPlayerBeginInterruption(player: AVAudioPlayer!) {
        /* The audio session is deactivated here */
    }

    func audioPlayerEndInterruption(player: AVAudioPlayer!, withOptions flags: Int) {
        if flags == AVAudioSessionInterruptionFlags_ShouldResume {
            player.play()
        }
    }

    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        if flag {
            NSLog("Audio player stopped correctly")
        } else {
            NSLog("Audio player did not stop correctly")
        }
        audioPlayer = nil
    }

    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        if flag {
            var file = currentFile
            NSLog("Successfully stopped the audio recording process to file \(file)")
            delegate?.audioRecordCreated(file)
            if (isRecording) {
                startRecordingAudio()
            }
        } else {
            NSLog("Stopping the audio recording failed")
        }
    }


    func audioRecordingPath() -> NSURL {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd_HH-mm-ss"
        currentFile = "\(dateFormatter.stringFromDate(NSDate())).m4a"
        return documentsUrl.URLByAppendingPathComponent(currentFile)
    }

    func audioPlayingPath(file: String) -> NSURL {
        return documentsUrl.URLByAppendingPathComponent("\(file)")
    }

    func audioRecordingSettings() -> [NSObject:AnyObject] {
        return [
                AVFormatIDKey: recordFormat,
                AVSampleRateKey: recordSampleRate,
                AVNumberOfChannelsKey: recordChannels,
                AVEncoderAudioQualityKey: recordQuality
        ]
    }

    func startRecordingAudio() {
        var error: NSError?
        let audioRecordingURL = self.audioRecordingPath()
        audioRecorder = AVAudioRecorder(URL: audioRecordingURL,
                settings: audioRecordingSettings(),
                error: &error)
        if let recorder = audioRecorder {
            recorder.delegate = self
            if recorder.prepareToRecord() && recorder.record() {
                NSLog("Successfully started to record.")
                let delayInSeconds = recordLength
                let delayInNanoSeconds =
                dispatch_time(DISPATCH_TIME_NOW,
                        Int64(delayInSeconds * Double(NSEC_PER_SEC)))
                dispatch_after(delayInNanoSeconds, dispatch_get_main_queue(), {
                    [weak self] in
                    self!.audioRecorder!.stop()
                })
            } else {
                NSLog("Failed to record.")
                audioRecorder = nil
            }
        } else {
            NSLog("Failed to create an instance of the audio recorder")
        }
    }


    func setupNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                selector: "interruption:",
                name: AVAudioSessionInterruptionNotification,
                object: nil)
    }

    func interruption(notification: NSNotification) {
        let why: AnyObject? =  notification.userInfo?[AVAudioSessionInterruptionTypeKey]

        if let why = why as? UInt{
            if let why = AVAudioSessionInterruptionType(rawValue: why){
                if why == .Began {
                    NSLog("interruption begin")
                    stopRecordingAudio()
                } else {
                    NSLog("interruption end")
                    startRecordingAudio()
                }
            }
        }
    }
}
