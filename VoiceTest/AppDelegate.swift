//
//  AppDelegate.swift
//  VoiceTest
//
//  Created by Admin on 23.04.15.
//  Copyright (c) 2015 nabsky. All rights reserved.
//


import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var testNavigationController: UINavigationController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        let appDefaults = [
                "Record Length": NSNumber(double: 10.0),
                "Record Quality": AVAudioQuality.Low.rawValue as NSNumber,
                "Record Channels": 1 as NSNumber,
                "Record Sample Rate": NSNumber(double: 16000.0),
                "Record Format": kAudioFormatMPEG4AAC as NSNumber
        ]
        NSUserDefaults.standardUserDefaults().registerDefaults(appDefaults)

        var navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(netHex:0xffffff)
        navigationBarAppearace.barTintColor = UIColor(netHex:0xD15353)
        navigationBarAppearace.titleTextAttributes =
                [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: CGFloat(20))!]


        UIApplication.sharedApplication().statusBarStyle = .LightContent

        testNavigationController = UINavigationController()

        var listViewController: UIViewController = ListViewController()
        self.testNavigationController!.pushViewController(listViewController, animated: false)
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.rootViewController = testNavigationController
        self.window!.backgroundColor = UIColor.whiteColor()
        self.window!.makeKeyAndVisible()

        return true
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        if DBSession.sharedSession().handleOpenURL(url) {
            if DBSession.sharedSession().isLinked() {
                println("Successfully Logged In")
            }
            return true
        }
        return false
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

    }


    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    }


    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }


    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }


    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}
