//
// Created by Admin on 26.04.15.
// Copyright (c) 2015 nabsky. All rights reserved.
//

import Foundation
import UIKit

class RecordCell: UITableViewCell {
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var fileNameLabel: UILabel = UILabel()
    var fileSizeLabel: UILabel = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.fileNameLabel = UILabel(frame: CGRect(x: 10.00, y: 5.00, width: screenSize.width - 10.00, height: 20.00));
        self.fileNameLabel.font = UIFont(name: "Avenir-Heavy", size: CGFloat(16))
        self.fileNameLabel.textColor = UIColor(netHex:0x32828E)//UIColor(netHex:0x333333)
        self.addSubview(self.fileNameLabel)

        self.fileSizeLabel = UILabel(frame: CGRect(x: 10.00, y: 22.00, width: screenSize.width - 10.00, height: 20.00));
        self.fileSizeLabel.font = UIFont(name: "Avenir-Book", size: CGFloat(12))
        self.fileSizeLabel.text = "size: unknown"
        self.addSubview(self.fileSizeLabel)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
