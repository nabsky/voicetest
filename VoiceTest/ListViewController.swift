import UIKit
import AVFoundation

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
        DBRestClientDelegate, AudioControllerDelegate {
    //TODO split in separate controllers

    let appKey = "v8s8jyxkejtrhwu"
    let appSecret = "r7oavzs5rpcyhkh"
    let appRoot = kDBRootAppFolder

    var dropboxSession = DBSession()
    var restClient = DBRestClient()

    let screenSize: CGRect = UIScreen.mainScreen().bounds
    let documentsUrl = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as! NSURL

    var files: [String] = []

    var recordButton: UIButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
    var tableView: UITableView = UITableView()

    var audioController = AudioController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UIDevice.currentDevice().batteryMonitoringEnabled = true
        initDropBox()
        initFileList()
        initControllers()
        createUI()
        loadSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initControllers(){
        audioController.delegate = self
    }


    func initDropBox() {
        dropboxSession = DBSession(appKey: appKey, appSecret: appSecret, root: appRoot)
        DBSession.setSharedSession(dropboxSession)

        if !DBSession.sharedSession().isLinked() {
            DBSession.sharedSession().linkFromController(self)
            //TODO check why it's not working at first run
        } else {
            //DBSession.sharedSession().linkFromController(self)
        }

        restClient = DBRestClient(session: DBSession.sharedSession())
        restClient.delegate = self
    }

    func upload(file: String) {
        var destDir = "/"
        NSLog("uploading \(file)")
        var fileURL = documentsUrl.URLByAppendingPathComponent("\(file)")

        var localDir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var localpath = localDir[0] as! String
        localpath = localpath + "/" + file

        NSLog("Trying to upload file: \(localpath)")
        restClient.uploadFile(file, toPath: destDir, withParentRev: nil, fromPath: localpath)
    }

    func restClient(client: DBRestClient!, uploadedFile destPath: String!, from srcPath: String!, metadata: DBMetadata!) {
        NSLog("Successfully uploaded \(metadata.path)")
    }

    func restClient(client: DBRestClient!, uploadFileFailedWithError error: NSError!) {
        NSLog("Upload error: \(error)")
    }


    func initFileList() {
        if let directoryContents = NSFileManager.defaultManager().contentsOfDirectoryAtPath(documentsUrl.path!, error: nil) {
            NSLog("%@", directoryContents)
        }
        if let directoryUrls = NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsSubdirectoryDescendants, error: nil) {
            NSLog("%@", directoryUrls)
            files = directoryUrls.map() {
                $0.lastPathComponent
            }.filter() {
                $0.pathExtension == "m4a"
            }
        }
    }

    func createUI() {
        self.title = "The Voices I Hear"

        recordButton.frame = CGRectMake(0, screenSize.height - 50, screenSize.width, 50)
        recordButton.backgroundColor = UIColor(netHex:0x32828E)
        recordButton.tintColor = UIColor.whiteColor()
        recordButton.titleLabel?.font = UIFont(name: "AvenirNext-UltraLight", size: CGFloat(30))
        recordButton.setTitle("Start Recording", forState: UIControlState.Normal)
        recordButton.addTarget(self, action: "recordAction:", forControlEvents: UIControlEvents.TouchUpInside)

        tableView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height - 50);
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")

        view.addSubview(tableView)
        view.addSubview(recordButton)
    }

    func recordAction(sender: UIButton!) {
        if (audioController.isRecording) {
            NSLog("Stop recording")
            audioController.stopRecordingAudio()
            recordButton.backgroundColor = UIColor(netHex:0x32828E)
            recordButton.setTitle("Start Recording", forState: UIControlState.Normal)
        } else {
            NSLog("Start recording")
            audioController.start()
            recordButton.backgroundColor = UIColor(netHex:0xD15353)
            recordButton.setTitle("Stop", forState: UIControlState.Normal)
        }
    }

//======================================================================================================================

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? RecordCell
        if cell == nil {
            tableView.registerClass(RecordCell.classForCoder(), forCellReuseIdentifier: "cell")
            cell = RecordCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        if var label = cell?.fileNameLabel {
            label.text = files[indexPath.row]
        } else {
            cell?.textLabel?.text = files[indexPath.row]
        }
        return cell!
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        NSLog("Click on cell #\(indexPath.row)!")

        if audioController.isRecording == true {
            let alertController = UIAlertController(title: "Want to play file?", message:
            "Please, stop the record first!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            audioController.play(files[indexPath.row])
        }
    }

//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if (editingStyle == UITableViewCellEditingStyle.Delete) {
//            let currentCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!;
//            let file = currentCell.textLabel!.text
//            if deleteRecord(file!) {
//                initFileList()
//            }
//        }
//    }

//======================================================================================================================
//    func deleteRecord(file: String) -> Bool{
//        var error:NSError?
//        let ok:Bool = NSFileManager.defaultManager().removeItemAtPath(audioPlayingPath(file).path!, error: &error)
//
//        if error != nil {
//            NSLog(error)
//        }
//
//        return ok;
//    }
//======================================================================================================================
    func loadSettings() {
        audioController.recordLength = NSUserDefaults.standardUserDefaults().doubleForKey("Record Length")
        audioController.recordQuality = NSUserDefaults.standardUserDefaults().integerForKey("Record Quality")
        audioController.recordChannels = NSUserDefaults.standardUserDefaults().integerForKey("Record Channels")
        audioController.recordSampleRate = NSUserDefaults.standardUserDefaults().doubleForKey("Record Sample Rate")
        audioController.recordFormat = NSUserDefaults.standardUserDefaults().integerForKey("Record Format")

        var settings:String = "Settings loaded.\n" +
                "Length: \(audioController.recordLength) \n" +
                "Quality: \(audioController.recordQuality) \n" +
                "Channels: \(audioController.recordChannels) \n" +
                "Sample Rate: \(audioController.recordSampleRate) \n" +
                "Format: \(audioController.recordFormat)"

        NSLog(settings)
    }

    func audioRecordCreated(filename: String) {
        files.append(filename)
        upload(filename)
        dispatch_async(dispatch_get_main_queue(), {
            () -> Void in
            self.tableView.reloadData()
        })
    }
}
