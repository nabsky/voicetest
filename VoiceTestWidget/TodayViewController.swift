//
//  TodayViewController.swift
//  VoiceTestWidget
//
//  Created by Admin on 26.04.15.
//  Copyright (c) 2015 nabsky. All rights reserved.
//

import UIKit
import NotificationCenter

@objc (TodayViewController)

class TodayViewController: UIViewController, NCWidgetProviding {
    var recordButton: UIButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton

    override func loadView() {
        view = UIView(frame:CGRect(x:0.0, y:0.0, width:320.0, height:200.0))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSizeMake(320, 200)
        createUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }


    func createUI() {
        recordButton.frame = CGRectMake(0, 0, 320, 50)
        recordButton.backgroundColor = UIColor.greenColor()
        recordButton.tintColor = UIColor.whiteColor()
        recordButton.setTitle("Start", forState: UIControlState.Normal)
        recordButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        view.addSubview(recordButton)
    }

    func buttonAction(sender: UIButton!) {
        println("Button tapped")
    }

}
